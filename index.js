require('dotenv').config();

const http                      = require('http');
const express                   = require('express');
const cors                      = require('cors');
const bodyParser                = require('body-parser');
const MessagingResponse         = require('twilio').twiml.MessagingResponse;
const request                   = require('request-promise-native');

const config = {
	"port"             : process.env.SMS_PROXY_PORT,
	"bodyLimit"        : "100kb"
};

const SOS_URL       = 'https://sos-barnebyer.now.sh/';
const BISNODE_URL   = 'https://api.bisnode.no/search/norway/directory/';
const AUTH_DETAILS  = Buffer.from(`${process.env.BISNODE_USER}:${process.env.BISNODE_PASSWORD}`).toString('base64');

const app = express();

app.server = http.createServer(app);

app.use(cors());

app.use(bodyParser.urlencoded({ extended: false }));

/*
  twilio webhook uses this endpoint as its callback
  TODO: implement logic,error handling, tests
*/
app.post('/', async (req, res) => {
  const twiml         = new MessagingResponse();
  const sender        = req.body;
  const lookupProp    = sender.From.substring(3);

  const params = {
    url: BISNODE_URL + lookupProp,
    headers: { 'Authorization': 'Basic ' + AUTH_DETAILS }
  };

  console.log(params);

  try {
    const response  = await request(params);
    const hits      = JSON.parse(response);

    console.log(JSON.stringify(hits, null, 4));

    const firstHit = hits.Result && hits.Result[0] ? hits.Result[0] : null;
    var base64encodedUser = null;

    // cant find any user in bisnode
    if(!firstHit) {
      res.writeHead(200, { 'Content-Type': 'text/xml' });
      twiml.message(`https://sos-barnebyer.now.sh?price=500`);
      res.end(twiml.toString());
    } else {
      if(firstHit.type === "Person") {
        const firstName   = firstHit.firstname  || '';
        const lastName    = firstHit.lastname   || '';
        const fullName    = `${firstName} ${lastName}`;
        const street      = firstHit.streetname || '';
        const houseNr     = firstHit.houseno    || '';
        const zipCode     = firstHit.zipcode    || '';
        const city        = firstHit.city       || '';
        const born        = firstHit.born       || '';
        const phone       = firstHit.mobile ? firstHit.mobile : firstHit.Phone[0].number;
        const email       = `${firstName.replace(/ /g,'')}.${lastName.replace(/ /g,'')}@konsent.id`;
        const address     = `${street} ${houseNr} ${zipCode} ${city}`;
        const user        = {name: fullName, phone, address, email};
        base64encodedUser = Buffer.from(JSON.stringify(user)).toString('base64');
      } else if (firstHit.type === "Unknown") {
        const fullName = "Jon Ramvi";
        const address  = "Gamlelinja 22";
        let phone  = "Reservert";
        const email = "Reservert@konsent.id";
        if(firstHit.Phone && firstHit.Phone[0]) {
          phone = firstHit.Phone[0].number;
        }
        const user = {name: fullName, phone, address, email};
        base64encodedUser = Buffer.from(JSON.stringify(user)).toString('base64');
      } else if(firstHit.type === "Hybrid") {
        const firstName   = firstHit.firstname  || '';
        const lastName    = firstHit.lastname   || '';

        const fullName    = fistName && lastName ? `${firstName} ${lastName}` : 'Jon Ramvi';
        const email       = firstName && lastName ? `${firstName.replace(/ /g,'')}.${lastName.replace(/ /g,'')}@konsent.id` : `jon.ramvi@konsent.id`;
        let phone         = firstHit.mobile;

        if(firstHit.Phone && firstHit.Phone[0]) {
          phone = firstHit.Phone[0].number;
        }
        const user = {name: fullName, phone, address, email};
        base64encodedUser = Buffer.from(JSON.stringify(user)).toString('base64');
      }

    if (req.body.Body == 'SOS 300') {
      twiml.message(`https://sos-barnebyer.now.sh?price=300&user=${base64encodedUser}`);
    } else if (req.body.Body == 'RK 199') {
      twiml.message(`https://sos-barnebyer.now.sh?price=300&user=${base64encodedUser}`);
    } else {
      twiml.message(`https://sos-barnebyer.now.sh?price=375&user=${base64encodedUser}`);
    }
      res.writeHead(200, { 'Content-Type': 'text/xml' });
      res.end(twiml.toString());
    }
  } catch (error) {
    res.writeHead(200, { 'Content-Type': 'text/xml' });
    twiml.message('couldnt find user');
    res.end(twiml.toString());
  }
});

app.use((err, req, res, next) => {
  console.error(err.stack)
  res.status(500).send('internal server error, report to torbjorn@blockchangers.com');
});

const server = app.listen(process.env.PORT || config.port, (port) => {
  console.log('sms proxy with twilio webhook callbak up and running at port: ', process.env.PORT || config.port)
});
