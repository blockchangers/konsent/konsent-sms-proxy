#!/usr/bin/env node
require('dotenv').config();

if (process.env.NODE_ENV === 'production') {
  throw new Error("Do not use nodemon in production, run index.js directly instead");
}

const nodemon = require('nodemon');
const ngrok   = require('ngrok');

// We start an ngrok tunnel to ensure it stays the same for the entire process
ngrok.connect({
  addr: process.env.PORT || process.env.SMS_PROXY_PORT,
  subdomain: process.env.TUNNEL|| 'konsent2', //available tunnel domains (konsent, konsent2)
  authToken: process.env.NGROK_AUTH_TOKEN,
  region: 'eu'
  }).then(ngrokUrl => {
    console.log(`
    _____ _____ _____    _____ _____ _____ __ __ __ __
    |   __|     |   __|  |  _  | __  |     |  |  |  |  |
    |__   | | | |__   |  |   __|    -|  |  |-   -|_   _|
    |_____|_|_|_|_____|  |__|  |__|__|_____|__|__| |_|

                                               `)
  console.log(`SMS proxy server ${ngrokUrl}`)
  nodemon(`-x 'NGROK_URL=${ngrokUrl} node' index.js`);
}).catch(err => {
  console.error(err);
  process.exit(1);
});

nodemon.on('start', () => {
  console.log('App has started');
}).on('quit', () => {
  console.log('App has quit');
}).on('restart', (files) => {
  console.log('App restarted due to: ', files);
});
